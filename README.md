## Overview

This Flask application provides an example of how to implement server-side sessions in Flask using [Flask-Session](https://flask-session.readthedocs.io/en/latest/) and [Redis](https://redis.io).

This project is utilized in the following blog post on [TestDriven.io](https://testdriven.io/blog/):

[Server-side Sessions in Flask with Redis](https://testdriven.io/blog/)

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-server-side-sessions.git
```

Create a new virtual environment:

```sh
$ cd flask-server-side-sessions
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

### Installation Instructions - Python Packages

**IMPORTANT** As of December 2023, there are compatibility issues between Flask 3.0.x (specifically, Werkzeug 3.x) and Flask-Session 0.5. The verions of the Python packages that work together for the example code in this tutorial are:

```
Flask==2.3.3
Flask-Session==0.5.0
redis==5.0.1
Werkzeug==2.3.8
```

Install the python packages:

```sh
(venv)$ pip install Flask==2.3.3
(venv)$ pip uninstall Werkzeug
(venv)$ pip install Werkzeug==2.3.8
(venv)$ pip install Flask-Session==0.5.0
(venv)$ pip install redis
```

## Redis

Spin up a Redis container using Docker (in a separate terminal window from the Flask application):

```sh
$ docker run --name flask-redis-db -d -p 6379:6379 redis
```

After you finish using the Redis database:

```sh
$ docker ps
$ docker rm <ID_OF_REDIS_CONTAINTER>
```

## How to Run

In the top-level directory, run development server to serve the Flask application:

```sh
(venv) $ flask --app app --debug run
```

Navigate to 'http://127.0.0.1:5000/get_email' to view the website!

## Configuration

The following configuration parameters are used in the Flask application:

* SECRET_KEY - used to cryptographically-sign the cookies used for storing the session identifier
* PERMANENT_SESSION_LIFETIME - sets how long the session should persist.  This setting needs to be used in conjunction with setting the `session` object: `session.permanent`
* SESSION_TYPE - specifies which type of session interface to use
* SESSION_PERMANENT - indicates whether to use permanent sessions (defaults to `True`)
* SESSION_USE_SIGNER - indicates whether to sign the session cookie identifier (defaults to `False`)
* SESSION_REDIS - specifies the Redis instance (default connection is to `127.0.0.1:6379`)

> Refer to the [Configuration](https://flask-session.readthedocs.io/en/latest/config.html) section of the Flask-Session documentation for details on all available configuration variables.

### Secret Key

The 'SECRET_KEY' can be generated using the following commands (assumes Python 3.6 or later):

```sh
(venv) $ python -c 'import secrets; print(secrets.token_hex())'
(venv) $ export SECRET_KEY=<secret_key_generated_above>
```

NOTE: If working on Windows, use `set` instead of `export`.

## Key Python Modules Used

- Flask - micro-framework for web application development
- Flask-Session - Flask extensions for implementing server-side session
- redis - Python wrapper for working with Redis data store

This application is written using Python 3.11 and Flask 2.3.3.
